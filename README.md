# Selenium Docker Bootstrapper

Build a "run" environment for executing Selenium scripts in a Docker container with minimal fuss about setting up Selenium driver.

## Usage
1. Edit `docker-compose.yml` and update selenium-app service's container_name to describe your app.
   1. If you wish to run multiple instances of this framework then change the selenium-chrome container_name too.
2. Edit `selenium_app/app.py` and insert or call your code in the `def app()` method.
   1. Use `selenium_app/settings.py` for your variables (aka data).
   2. Add needed Python packages to `requirements.txt` and they will be installed into your container.
3. Log file will be located at `selenium_app/logs/output.log` by default.
4. Depending on how your app works your app's container could close down when it has finished running your script.
