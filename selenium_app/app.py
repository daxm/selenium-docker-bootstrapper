import logging

import settings
from selenium.webdriver.common.by import By


def app(driver):
    logging.info(f"Starting {__name__}.")

    # ##### #
    # Do something with driver. (AKA, put your code here.)
    driver.get(settings.URL)
    elements = driver.find_elements(By.XPATH, "//div")
    for element in elements:
        logging.debug(element.text)
    # ##### #

    logging.info(f"Ending {__name__}.")
