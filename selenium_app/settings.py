# App Variables
URL = "https://daxm.net"

# Selenium Container Related Variables
STANDALONE_WAIT = 15
SELENIUM_URL = "http://selenium-chrome:4444/wd/hub"

# Logging Variables
LOG_DIRECTORY = "logs"
LOG_FILENAME = "output.log"
LOG_FORMAT = "%(asctime)-7s: %(levelname)-1s %(message)s"
LOG_DATE_FORMAT = "%Y-%m-%d %H:%M:%S"
# LOG_LEVEL = "DEBUG"
LOG_LEVEL = "INFO"
