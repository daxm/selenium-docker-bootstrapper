import logging
import os
import time

import settings
from app import app
from selenium import webdriver
from selenium.webdriver.chrome.options import Options

LOGGING_FILE_PATH = os.path.join(
    os.getcwd(),
    settings.LOG_DIRECTORY,
    settings.LOG_FILENAME,
)
logging.basicConfig(
    level=settings.LOG_LEVEL or logging.INFO,
    format=settings.LOG_FORMAT,
    datefmt=settings.LOG_DATE_FORMAT,
    handlers=[
        logging.FileHandler(LOGGING_FILE_PATH, "w+"),
        logging.StreamHandler(),
    ],
)


def set_chrome_options() -> Options:
    """
    Sets chrome options for Selenium.

    Chrome options for headless browser is enabled.
    """
    chrome_options = Options()
    ''' I don't think any of these are needed when using the selenium/standalone-chrome container.
    chrome_options.add_argument("--headless")
    chrome_options.add_argument("--no-sandbox")
    chrome_options.add_argument("--disable-dev-shm-usage")
    chrome_prefs = {}
    chrome_options.experimental_options["prefs"] = chrome_prefs
    chrome_prefs["profile.default_content_settings"] = {"images": 2}
    '''
    return chrome_options


if __name__ == "__main__":
    # Wait for selenium/standalone-chrome container to load.
    logging.info(
        f"Waiting {settings.STANDALONE_WAIT}s for selenium/standalone container to fully load."
    )
    time.sleep(settings.STANDALONE_WAIT)

    # Set up driver to connect to selenium/standalone-chrome container running locally
    logging.debug("Setting up driver.")
    driver = webdriver.Remote(
        command_executor=settings.SELENIUM_URL,
        options=set_chrome_options(),
    )

    # Run selenium_app
    logging.debug("Calling app.")
    app(driver=driver)

    # Quit driver
    logging.debug("Quiting driver.")
    driver.quit()
