#!/usr/bin/env bash

echo "--> Starting, if necessary, selenium/standalone container."
docker run -d -p 4444:4444 -p 7900:7900 --shm-size="2g" -e "SE_NODE_MAX_SESSIONS=10" selenium/standalone-chrome
echo

echo "--> Update GIT repo"
git pull
echo

echo "--> Build and run docker-compose referenced containers"
docker-compose up --build --remove-orphans -d
echo

echo "--> Prune Images:"
docker image prune -a -f
echo

echo "--> Docker Status"
docker ps
echo
