FROM python

COPY requirements.txt .
RUN pip install --upgrade pip
RUN pip install -r requirements.txt

WORKDIR /app

CMD ["python", "./bootstrap.py"]
